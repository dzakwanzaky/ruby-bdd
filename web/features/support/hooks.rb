Before do |scenario|
    # we use this when call .load function
    Capybara.app_host = ENV['BASE_URL']
  
    Capybara.default_max_wait_time = DEFAULT_TIMEOUT
    Capybara.javascript_driver = :chrome
    Selenium::WebDriver.logger.level = :debug
    Selenium::WebDriver.logger.output = 'selenium.log'
  
    # another option to setup driver, for now we define it on env.rb
    # Capybara.javascript_driver = :chrome_headless if ENV['BROWSER'].eql? 'chrome_headless'
    # Capybara.javascript_driver = :chrome if ENV['BROWSER'].eql? 'chrome'
  
    @pages = App.new
    @load_data = LoadData.new
    @tags = scenario.source_tag_names
end