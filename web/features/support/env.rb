require 'capybara'
require 'capybara/cucumber'
require 'cucumber'
require 'dotenv'
require 'rspec'
require 'webdrivers'
require 'data_magic'
require 'site_prism'

Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome, timeout: 30)
end

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.default_max_wait_time = 30
end

require_relative '../lib/base_helper'

include BaseHelper

# to load environment variable on .env file into ENV variable
Dotenv.load

# set data magic default directory
DataMagic.yml_directory = './features/config'

DEFAULT_TIMEOUT = ENV['DEFAULT_TIMEOUT'].to_i